import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import './index.css';
import Main from "./pages/Main";
import Upload from "./pages/Upload";
import Retrieve from "./pages/Retrieve";
import KeyNotFound from "./pages/KeyNotFound";
import SuccessUpload from "./pages/SuccessUpload";

ReactDOM.render(
    <Router>
        <Switch>
            <Route exact path="/" component={Main} />
            <Route path="/upload" component={Upload} />
            <Route path="/retrieve" component={Retrieve} />
            <Route path="/error" component={KeyNotFound} />
            <Route path="/success" component={SuccessUpload} />
        </Switch>
    </Router>,
    document.getElementById('root')
);