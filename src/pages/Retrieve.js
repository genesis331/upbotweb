import React, {useEffect, useState} from 'react';
import { Redirect } from "react-router-dom";
import queryString from 'query-string';
import '../styles/retrieve.css';
import logo from "../assets/UpBot_Logo_Cropped.png";

export default function Retrieve (props) {
    const [uploadKey, setUploadKey] = useState("");
    const [errorRedirect, setErrorRedirect] = useState(false);
    const [disabled, setDisabled] = useState("disabled");
    const [downloadSrc, setDownloadSrc] = useState("");

    function getLink(key) {
        let xhr = new XMLHttpRequest();
        xhr.addEventListener('load', () => {
            if (xhr.status === 200) {
                setDisabled("");
                setDownloadSrc(JSON.parse(xhr.response).downloadLink);
            }
        });
        xhr.open('GET', 'https://cors-anywhere.herokuapp.com/https://upbot-cloud.herokuapp.com/retrieve?key=' + key);
        xhr.send();
    }

    useEffect(() => {
        let xhr = new XMLHttpRequest();
        let params = queryString.parse(props.location.search);
        setUploadKey(params.key);
        xhr.addEventListener('load', () => {
            if (xhr.responseText !== 'OK') {
                setErrorRedirect(true);
            } else if (xhr.responseText === 'OK') {
                getLink(params.key);
            }
        });
        xhr.open('GET', 'https://cors-anywhere.herokuapp.com/https://upbot-cloud.herokuapp.com/verify?key=' + params.key);
        xhr.send();
    },[props.location.search]);

    let redirectElem;
    if (errorRedirect) {
        redirectElem = <Redirect to={'/error'}/>
    }

    return (
        <div className="retrieve-content">
            {redirectElem}
            <div className="retrieve-content-box">
                <div className="logo">
                    <img src={logo} alt="logo" draggable="false" />
                </div>
                <div className="bot-name">
                    <h1>
                        upBot
                    </h1>
                </div>
                <div className="upload-key">
                    upBot Link Key: {uploadKey}
                </div>
                <div className="download-sec">
                    <button className={disabled} onClick={() => {window.location.href = downloadSrc}}>Download File</button>
                </div>
            </div>
        </div>
    )
}