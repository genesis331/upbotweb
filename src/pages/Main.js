import React from 'react';
import '../styles/main.css';
import logo from '../assets/UpBot_Logo_Cropped.png';
import discordLogo from '../assets/Discord-Logo-Color.svg';
import gitlabLogo from '../assets/gitlab-icon-rgb.svg';

export default function Main () {
    return (
        <div className="main-content">
            <div className="content-container">
                <section className="sec1">
                    <div className="sec1-content">
                        <div>
                            <img src={logo} alt="logo" draggable="false" />
                        </div>
                        <div>
                            <h1>
                                upBot
                            </h1>
                        </div>
                        <div>
                            <p>
                                A Discord bot used to upload files to an external server and have their links shared automatically.
                            </p>
                        </div>
                        <div>
                            <button>Invite Bot</button>
                            <button>See Commands</button>
                        </div>
                        <div>
                            <iframe title="widget" src="https://discordapp.com/widget?id=706068257991295006&theme=dark" width="80%" height="200" frameBorder="0">

                            </iframe>
                        </div>
                        <div>
                            <h3>Found an issue? Help us report it!</h3>
                            <a href={'https://discord.gg/JEk2hcJ'} target="_blank" rel="noopener noreferrer">
                                <img src={discordLogo} alt="discordLogo" draggable="false" />
                            </a>
                            <a href={'https://gitlab.com/yenn01/upbot/-/issues'} target="_blank" rel="noopener noreferrer">
                                <img src={gitlabLogo} alt="gitlabLogo" draggable="false" />
                            </a>
                        </div>
                    </div>
                </section>
                <section className="sec2">
                    <div className="sec2-contents">
                        <div className="sec2-content-box1">

                        </div>
                        <div className="sec2-content-box2">
                            <div className="sec2-content-box2a">
                                <div className="sec2-content-box2a-center">
                                    <h1>Required Server Permissions</h1>
                                    <br/>
                                    <div>
                                        <h2>
                                            Send Messages
                                        </h2>
                                        <p>
                                            Allows for sending messages in a channel.
                                        </p>
                                    </div>
                                    <div>
                                        <h2>
                                            Embed Links
                                        </h2>
                                        <p>
                                            Links sent by bots with this permission will be auto-embedded.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="sec2-content-box2b">
                                <div>
                                    <div className="sec2-content-box2ba-center">
                                        <h1>Contributors</h1>
                                        <div className="sec2-content-box2ba-pic">
                                            <a href="https://gitlab.com/yenn01" target="_blank" rel="noreferrer noopener"><img src="https://gitlab.com/uploads/-/system/user/avatar/3760610/avatar.png" alt="contributor" draggable={false}/></a>
                                            <a href="https://gitlab.com/genesis331" target="_blank" rel="noreferrer noopener"><img src="https://gitlab.com/uploads/-/system/user/avatar/1908619/avatar.png" alt="contributor" draggable={false}/></a>
                                        </div>
                                    </div>
                                </div>
                                <div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    )
}