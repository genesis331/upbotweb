import React from 'react';
import '../styles/result.css';
import logo from "../assets/UpBot_Logo_Cropped.png";
import checkedCircle from "../assets/checked-circle.svg";

export default function KeyNotFound () {
    return (
        <div className="result-content">
            <div className="result-content-box">
                <img src={checkedCircle} alt="checkedCircle" draggable={false} />
                <h1>
                    Done Uploading!
                </h1>
                <p>
                    200: File uploaded to server.
                </p>
            </div>
            <footer>
                <img src={logo} alt="logo" draggable={false} />
            </footer>
        </div>
    )
}