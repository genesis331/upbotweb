import React from 'react';
import '../styles/result.css';
import logo from "../assets/UpBot_Logo_Cropped.png";
import xCircle from "../assets/x-circle.svg";

export default function KeyNotFound () {
    return (
        <div className="result-content">
            <div className="result-content-box">
                <img src={xCircle} alt="checkedCircle" draggable={false} />
                <h1>
                    There was an error.
                </h1>
                <p>
                    400: Bad request.
                </p>
            </div>
            <footer>
                <img src={logo} alt="logo" draggable={false} />
            </footer>
        </div>
    )
}