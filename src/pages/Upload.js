import React, {useCallback, useEffect, useState} from 'react';
import { Redirect } from "react-router-dom";
import {useDropzone} from 'react-dropzone';
import queryString from 'query-string';
import '../styles/upload.css';
import logo from "../assets/UpBot_Logo_Cropped.png";
import inboxLogo from "../assets/inbox.svg";

export default function Upload (props) {
    const [uploadKey, setUploadKey] = useState("");
    const [errorRedirect, setErrorRedirect] = useState(false);
    const [successRedirect, setSuccessRedirect] = useState(false);
    const [textContent, setTextContent] = useState("");

    const onDrop = useCallback(acceptedFile => {
        setTextContent('Uploading file...');
        let params = queryString.parse(props.location.search);
        let data = new FormData();
        data.append("file", acceptedFile[0], acceptedFile[0].name);
        let xhr = new XMLHttpRequest();
        xhr.addEventListener('load', () => {
            if (xhr.responseText === 'OK') {
                setSuccessRedirect(true);
                console.log('Done');
            }
        });
        xhr.open('POST', 'https://cors-anywhere.herokuapp.com/https://upbot-cloud.herokuapp.com/upload?key=' + params.key);
        xhr.send(data);
    }, [props.location.search]);
    const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop})

    useEffect(() => {
        let xhr = new XMLHttpRequest();
        let params = queryString.parse(props.location.search);
        setUploadKey(params.key);
        xhr.addEventListener('load', () => {
            if (xhr.responseText !== 'OK') {
                setErrorRedirect(true);
            }
        });
        xhr.open('GET', 'https://cors-anywhere.herokuapp.com/https://upbot-cloud.herokuapp.com/verify?key=' + params.key);
        xhr.send();
    },[props.location.search]);

    let redirectElem;
    if (errorRedirect) {
        redirectElem = <Redirect to={'/error'}/>
    }
    let redirectElem2;
    if (successRedirect) {
        redirectElem2 = <Redirect to={'/success'}/>
    }

    return (
        <div className="upload-content">
            {redirectElem}
            {redirectElem2}
            <div className="upload-content-box">
                <div className="logo">
                    <img src={logo} alt="logo" draggable="false" />
                </div>
                <div className="bot-name">
                    <h1>
                        upBot
                    </h1>
                </div>
                <div className="upload-key">
                    upBot Link Key: {uploadKey}
                </div>
                <div className="upload-area">
                    <div className={isDragActive ? "isDragActive upload-box" : "upload-box"}>
                        <div className="upload-box-background">
                            <div className="upload-box-bg-center">
                                <img src={inboxLogo} alt="inbox" />
                                <p>
                                    <b>Choose a file</b> or drag it here.
                                    <br/>
                                    {textContent}
                                </p>
                            </div>
                        </div>
                        <div className="upload-box-receiver" {...getRootProps()}>
                            <input {...getInputProps()} />
                        </div>
                    </div>
                </div>
                {/*<div className="upload-button">*/}
                {/*    <button>Upload</button>*/}
                {/*</div>*/}
            </div>
        </div>
    )
}